package it.novasemantics.checkers.server;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Service;

import it.novasemantics.checkers.server.CheckersTable.Move;
import it.novasemantics.checkers.server.CheckersTable.Coordinate;
import it.novasemantics.checkers.server.CheckersTable.IrregularMove;
import it.novasemantics.checkers.server.CheckersTable.Player;

@Service
public class CheckersService {

	private Map<String, Match> matches = new HashMap<>();
	
	public String createMatch(String name) {
		Match m = new Match(name);
		String id = UUID.randomUUID().toString();
		matches.put(id, m);
		return id;
	}
	
	public Match getMatch(String id) {
		return matches.get(id);
	}
	
	public static class MatchNotFoundException extends Exception {}
	
	public void play(String id, Player p, Coordinate from, Coordinate to) throws MatchNotFoundException, IrregularMove{
		Match m = getMatch(id);
		if (m==null)
			throw new MatchNotFoundException();
		m.play(new Move(p, from,to));		
	}
	
}

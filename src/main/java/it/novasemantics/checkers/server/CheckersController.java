package it.novasemantics.checkers.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import it.novasemantics.checkers.server.CheckersService.MatchNotFoundException;
import it.novasemantics.checkers.server.CheckersTable.IrregularMove;
import it.novasemantics.checkers.server.CheckersTable.Move;

@RestController
@RequestMapping("matches")
public class CheckersController {

	@Autowired
	CheckersService s;
	
	@PostMapping("")
	public String createMatch(String name) {
		return s.createMatch("name");
	}
		
	@GetMapping("/{id}")
	public CheckersTable get(@PathVariable String id)
	{
		Match m = s.getMatch(id);
		if (m==null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		return m.getTable();
	}
	
	@PostMapping("/{id}/move")
	public CheckersTable play(@PathVariable String id, @RequestBody Move move) {
		try {
			s.play(id, move.getPlayer(), move.getFrom(), move.getTo());
			return get(id);
		} catch (MatchNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}	catch (IrregularMove e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}
}

package it.novasemantics.checkers.server;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckersServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CheckersServerApplication.class, args);
	}

	
	@PostConstruct
	private void init() {
		CheckersTable t = CheckersTable.init();
		System.out.println(t);
	}
}

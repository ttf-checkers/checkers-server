package it.novasemantics.checkers.server;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
public class CheckersTable implements Cloneable {

	@Override
	protected Object clone() throws CloneNotSupportedException {
		CheckersTable res = new CheckersTable();
		for (int r = 0; r < 8; r++)
			for (int c = 0; c < 8; c++)
				res.getTable()[r][c] = getTable()[r][c];
		res.currentPlayer = currentPlayer;
		return res;
	}

	public static enum Player {
		BLACK, WHITE
	}

	public static enum PieceType {
		PIECE, KING
	}

	@AllArgsConstructor
	@Getter
	public static class Coordinate {
		private int row, column;
	}

	@AllArgsConstructor
	@Getter
	public static class Move {
		private Player player;
		private Coordinate from, to;
	}

	@AllArgsConstructor
	@Getter
	@ToString
	public static class Piece {
		private Player player;
		@Setter
		private PieceType type;
	}

	private Player currentPlayer;

	// NOTA: [riga][colonna] angolo [0][0] in basso a sinistra
	// il bianco muove dal basso verso l'alto (per righe crescenti)
	@Getter
	Piece[][] table = new Piece[8][8];

	public static CheckersTable init() {
		CheckersTable t = new CheckersTable();
		for (int r = 0; r < 3; r++)
			for (int c = 0; c < 8; c++)
				if ((c + r) % 2 == 0)
					t.getTable()[r][c] = new Piece(Player.WHITE, PieceType.PIECE);
		for (int r = 5; r < 8; r++)
			for (int c = 0; c < 8; c++)
				if ((c + r) % 2 == 0)
					t.getTable()[r][c] = new Piece(Player.BLACK, PieceType.PIECE);
		t.setCurrentPlayer(Player.WHITE);
		return t;
	}

	public void set(Coordinate c, Piece p) {
		getTable()[c.getRow()][c.getColumn()] = p;
	}

	public Piece get(Coordinate c) {
		return getTable()[c.getRow()][c.getColumn()];
	}

	public Player getPlayer(Coordinate c) {
		Piece p = get(c);
		return p == null ? null : p.getPlayer();
	}

	public Player getWinner() {
		boolean b = false, w = false;
		for (int r = 0; r < 8; r++)
			for (int c = 0; c < 8; c++) {
				Player p = getPlayer(new Coordinate(r, c));
				b |= p == Player.BLACK;
				w |= p == Player.WHITE;
			}
		return !b ? Player.WHITE : !w ? Player.BLACK : null;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	public class IrregularMove extends Exception {
		@Getter
		private String message;
		private static final long serialVersionUID = -6835975315514240880L;
	}

	public CheckersTable move(Move m) throws IrregularMove {
		CheckersTable res = null;
		try {
			res = (CheckersTable) clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		if (m.getPlayer() != getCurrentPlayer())
			throw new IrregularMove("wrong player");
		if (m.getFrom() == null || m.getTo() == null)
			throw new IrregularMove();
		int r = m.getFrom().getRow();
		int c = m.getFrom().getColumn();
		int r2 = m.getTo().getRow();
		int c2 = m.getTo().getColumn();
		int dr = r2 - r;
		int dc = c2 - c;
		if (r < 0 || r > 7 || c < 0 || c > 7 || r2 < 0 || r2 > 7 || c2 < 0 || c2 > 7 || Math.abs(dr) != Math.abs(dc)
				|| dr == 0)
			throw new IrregularMove();

		Piece piece = get(m.getFrom());
		if (!(piece.getType() == PieceType.PIECE)
				&& (m.getPlayer() == Player.WHITE && dc < 0 || m.getPlayer() == Player.BLACK && dc > 0))
			throw new IrregularMove("wrong direction");

		if (get(m.getTo()) != null)
			throw new IrregularMove("confilct");

		if (dc > 1) {
			if (dc % 2 > 0)
				throw new IrregularMove();
			int ddc = dc > 0 ? 1 : -1;
			int ddr = dr > 0 ? 1 : -1;

			for (int i = 0; i < dc; i += 2) {
				Coordinate x = new Coordinate(r + i * ddr, c + i * ddc);
				if (res.get(x) == null || res.get(x).getPlayer() == m.getPlayer())
					throw new IrregularMove("capturing your own piece");
				x = new Coordinate(r + (i + 1) * ddr, c + (i + 1) * ddc);
				if (res.get(x) != null)
					throw new IrregularMove("conflict");
				res.set(x, null);
			}
		}

		res.set(m.getTo(), piece);
		res.set(m.getFrom(), null);
		if (r2 == 7 && m.getPlayer() == Player.WHITE || r2 == 0 && m.getPlayer() == Player.BLACK)
			res.set(m.getTo(), new Piece(piece.getPlayer(), PieceType.KING));
		res.setCurrentPlayer(res.getCurrentPlayer() == Player.BLACK ? Player.WHITE : Player.BLACK);
		return res;
	}

	public String toString() {
		String s = "\n\n";
		for (int r = 7; r >= 0; r--) {
			for (int c = 0; c < 8; c++)
				s += (r + c) % 2 != 0 ? "#####" : "     ";
			s += "\n";
			for (int c = 0; c < 8; c++) {
				Piece p = get(new Coordinate(r, c));
				String sp = (r + c) % 2 != 0 ? "#####" : "     ";
				if (p != null) {
					sp = p.getPlayer() == Player.WHITE ? "W" : "B";
					if (p.getType() == PieceType.KING)
						sp = " [" + sp + "] ";
					else
						sp = "  " + sp + "  ";
				}
				s += sp;
			}
			s += "\n";
			for (int c = 0; c < 8; c++)
				s += (r + c) % 2 != 0 ? "#####" : "     ";
			s += "\n";
		}
		s += "\n";
		return s;
	}
}

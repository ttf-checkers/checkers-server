package it.novasemantics.checkers.server;

import java.util.ArrayList;
import java.util.List;

import it.novasemantics.checkers.server.CheckersTable.IrregularMove;
import it.novasemantics.checkers.server.CheckersTable.Move;
import lombok.Getter;

@Getter
public class Match {
	
	private CheckersTable table=CheckersTable.init();
	private List<CheckersTable.Move> moves = new ArrayList<>();
	private List<CheckersTable> history = new ArrayList<>();
	
	@Getter
	private String name;
	
	public Match(String name) {
		this.name=name;
	}

	public void play(Move m) throws IrregularMove {
		CheckersTable t2 = table.move(m);
		history.add(table);
		moves.add(m);
		table = t2;
	}
	
}
